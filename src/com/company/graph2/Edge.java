package com.company.graph2;

public class Edge implements Comparable<Edge>{

    int firstEnd;
    int secondEnd;
    int weight;

    public Edge(int firstEnd, int secondEnd, int weight) {
        this.firstEnd = firstEnd;
        this.secondEnd = secondEnd;
        this.weight = weight;
    }

    public int getFirstEnd() {
        return firstEnd;
    }

    public void setFirstEnd(int firstEnd) {
        this.firstEnd = firstEnd;
    }

    public int getSecondEnd() {
        return secondEnd;
    }

    public void setSecondEnd(int secondEnd) {
        this.secondEnd = secondEnd;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(Edge o) {
        return this.weight - o.getWeight();
    }
}