package com.company.graph2;

import javafx.util.Pair;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();

    @Override
    public void dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        return;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int[] row;
        int result = 0;
        int value;
        int startPoint = 0;
        Set<Integer> dots = new HashSet<>();
        dots.add(startPoint);
        while(dots.size() != adjacencyMatrix.length)
        {
            int x = 0, y = 0;
            int minWeight = 1000000;
            for(int i : dots)
            {
                row = adjacencyMatrix[i];
                for(int j = 0; j < row.length; j++)
                {
                    value = row[j];
                    if((value < minWeight) && (value != 0) && (!dots.contains(j)))
                    {
                        x = i;
                        y = j;
                        minWeight = value;
                    }
                }

            }
            dots.add(y);
            dots.add(x);
            result += minWeight;
        }

        return result;
    }
    public boolean isBridge(boolean[][] adjacencyMatrix, int indexStart, int target) {
        ArrayList<Integer> result = new ArrayList<>();

        arrayDeque.offerFirst(indexStart);
        while (!arrayDeque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[indexStart][i] && !arrayDeque.contains(i) && !result.contains(i)) {
                    arrayDeque.offerFirst(i);
                }
            }
            result.add(arrayDeque.pollLast());
            if (!arrayDeque.isEmpty()) {
                indexStart = arrayDeque.getLast();
            }
        }
        return (result.contains(target));
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {

        int result = 0;
        TreeMap<Integer, List<Pair<Integer, Integer>>> edgesMap = new TreeMap<>();
        ArrayList<Pair<Integer, Integer>> arrayList;
        for (int i = 0; i < adjacencyMatrix.length; i++)
        {
            for (int j = i; j < adjacencyMatrix.length; j++)
            {
                if (adjacencyMatrix[i][j] > 0)
                {
                    if (!edgesMap.containsKey(adjacencyMatrix[i][j]))
                    {
                        arrayList = new ArrayList<>();
                        arrayList.add(new Pair<>(i, j));
                        edgesMap.put(adjacencyMatrix[i][j], arrayList);
                    }
                    else
                        {

                        arrayList = (ArrayList<Pair<Integer, Integer>>) edgesMap.get(adjacencyMatrix[i][j]);
                        arrayList.add(new Pair<>(i, j));
                        }
                }
            }
        }
        System.out.println(edgesMap);
        boolean[][] isWaysBetweenMatrix = new boolean[adjacencyMatrix.length][adjacencyMatrix.length];

        while (!edgesMap.isEmpty())
        {
            arrayList = (ArrayList<Pair<Integer, Integer>>) edgesMap.get(edgesMap.firstKey());
            for (Pair<Integer, Integer> item : arrayList)
            {
                if (!isBridge(isWaysBetweenMatrix, item.getKey(), item.getValue()))
                {
                    result += edgesMap.firstKey();
                    isWaysBetweenMatrix[item.getKey()][item.getValue()] = true;
                    isWaysBetweenMatrix[item.getValue()][item.getKey()] = true;

                }
            }
            edgesMap.remove(edgesMap.firstKey());
        }
        return result;
    }
}
